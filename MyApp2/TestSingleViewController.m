//
//  TestSingleViewController.m
//  green-machine
//
//  Created by momo on 10/23/12.
//
//

#import "TestSingleViewController.h"
#import "SBJson.h"
#import "SearchResultViewController.h"
#import "UserProfileData.h"
#import "AppDelegateProtocol.h"

@interface TestSingleViewController ()
@property NSArray* searchResult;
@end

@implementation TestSingleViewController
@synthesize QResult;

- (NSString*) XSDTripleStringToPureString:(NSString*)rawString
{
    NSArray* chunkedString = [rawString componentsSeparatedByString: @"'\'"];
    NSLog(@"Pure string %@",[chunkedString objectAtIndex:1]);
    
    return [chunkedString objectAtIndex:1];
}

- (UserProfileData*) theAppDataObject
{
    id<AppDelegateProtocol> theDelegate = (id<AppDelegateProtocol>) [UIApplication sharedApplication].delegate;
    UserProfileData* theDataObject;
    theDataObject = (UserProfileData*) theDelegate.theAppDataObject;
    
    return theDataObject;
    
}

- (IBAction)QueryToDatabase:(id)sender {
    
    //Target URL
    NSString* dbURL = [[NSString alloc] initWithFormat:@"http://128.113.106.31:10035/repositories/ios-hastens?query="];
    
    //Draft query
    NSString* query = [[NSString alloc] initWithFormat:@"prefix hastens:<http://ios-hastens/> select (str(?name) as ?result) where {?s hastens:has-member ?members. ?members hastens:hasName ?name. }"];
    
    //Encode query to UTF8
    query = (__bridge NSString *)CFURLCreateStringByAddingPercentEscapes(
                                                        NULL,
                                                        (CFStringRef)query,
                                                        NULL,
                                                        (CFStringRef)@"!*'();:@&=+$,/?%#[]",
                                                        kCFStringEncodingUTF8 );
    //Aggregated the query string
     NSString* aggregatedQuery = [dbURL stringByAppendingString:query];
    
    //Prepare reqeust
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:aggregatedQuery]];
    [request setHTTPMethod:@"GET"];
    
    //Set header
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    //Get response
    NSHTTPURLResponse* urlResponse = nil;
    NSError *error = [[NSError alloc] init];
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&urlResponse error:&error];
    NSString *result = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    
    NSLog(@"Response code:%d",[urlResponse statusCode]);
    if ([urlResponse statusCode]>=200 && [urlResponse statusCode] <300) {
        
        SBJsonParser *parser = [[SBJsonParser alloc] init];
        // 1. get the top level value as a dictionary
        NSDictionary *jsonObject = [parser objectWithString:result error:NULL];
        // 2. get the lessons object as an array
        NSArray *list = [jsonObject objectForKey:@"values"];
        // 3. iterate the array; each element is a dictionary...
        
        NSString* nameList = @"";
        
        NSLog(@"%@",[list description]);
        
        for (NSDictionary *names in list)
        {
            // 3 ...that contains a string for the key "stunde"
            nameList = [nameList stringByAppendingString:[names description]];
            //NSLog(@"%@",[names description]);
            nameList = [nameList stringByAppendingString:@"\n"];
            //NSLog(@"%@",[self XSDTripleStringToPureString:[names description]]);
            
        }
        
        //QResult.text = nameList;
        UserProfileData* theDataObject = [self theAppDataObject];
        theDataObject.searchResult = list;

    }
    
    SearchResultViewController* mySearchResult = [[SearchResultViewController alloc] initWithNibName:@"SearchResultViewController" bundle:nil];
    [self.navigationController pushViewController:mySearchResult animated:YES];
    
    
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setQDatabase:nil];
    [self setQResult:nil];
    [super viewDidUnload];
}
@end
